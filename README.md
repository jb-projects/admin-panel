# Kodilla Solo Project - Admin Panel

To start serving project (work in progress) type: `npm run serve`.  
To start serving styleguide (work in progress) type: `npm run serve-sg`.  
For the latest progress check `dev` branch.  
To build project type: `npm run build`. Latest build: http://kodilla-wd.bagnicki.net/admin-panel.  

**Made with [Parcel](https://github.com/parcel-bundler/parcel)**.

---

## TODO

### Components
- [x] Links
- [x] Buttons
- [x] Cards
- [x] Counter
- [ ] Alerts
- [ ] Tabs
- [ ] Table
- [ ] Slider
- [ ] Checkbox
- [ ] Other inputs
- [ ] Popups

### Layout
- [x] Sidebav
- [x] Topnav
- [ ] Mobile styles

### Pages
- [x] General
- [ ] Links & banners
- [ ] Personal data
- [ ] Details
- [ ] Payout
- [ ] Popups

### JS
- [x] Toggle sidebar
- [x] Fetching subpages
