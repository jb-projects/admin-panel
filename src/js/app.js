'use strict'

const UI = (function () {
  // UI vars
  const UISidebar = document.querySelector('#sidebar');
  const UIContentWrapper = document.querySelector('#content-wrapper');
  const UISidebarToggleBtn = document.querySelector('#sidebar-toggle-btn');
  UISidebarToggleBtn.addEventListener('click', toggleSidebar);
  const UINavLinks = document.querySelectorAll('.sidebar__nav-link');
  UINavLinks.forEach(link => link.addEventListener('click', togglePage));

  // PUBLIC toogleSidebar
  function toggleSidebar () {
    UISidebar.classList.toggle('hidden');
    UIContentWrapper.classList.toggle('full');
  }

  // PUBLIC togglePage
  function togglePage (e) {
    if (e.currentTarget !== document.querySelector('.sidebar__nav-link.js-active')) {
      UINavLinks.forEach(link => link.classList.remove('js-active'));
      e.currentTarget.classList.add('js-active');
      router.loadPage(e.currentTarget.href);
    }
    e.preventDefault();
  }

  return {
    toggleSidebar,
    togglePage
  }
})();

// Simple router mock
const router = (function () {
  // UI vars
  const UIContentWrapper = document.querySelector('#content-wrapper');

  // PUBLIC loadPage
  function loadPage(path) {
    fetch(path).then(res => res.text()
      .then(html => UIContentWrapper.innerHTML = html));
  }
  return {
    loadPage
  }
})();

// Load first page
document.querySelector('.sidebar__nav-link').click();


